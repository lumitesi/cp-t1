TESTES di110.di.fct.unl.pt
##############################################################################################
usage: cp.articlerep.MainRep time(sec) nthreads nkeys put(%) del(%) get(%) nauthors nkeywords nfindlist

nkeys - nr de palavras a considerar do dicionario [subset]
nauthors, nkeywords - valores medios por article
nfindlist - tamanho das listas para as pesquisas por autor e por keywords
##############################################################################################


java -cp bin  cp.articlerep.MainRep 10 16 100000 20 20 60 10 50 50

Total time: 10 seconds
Operation rate: 13973 ops/s


java -cp bin -Dcp.articlerep.validate=true cp.articlerep.MainRep 10 16 100000 20 20 60 10 50 50

Check done - Validate
Total time: 11 seconds
Operation rate: 12888 ops/s

_________________________________________________

Teste de referencia
_________________________________________________

java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 10

Total time: 10 seconds
Operation rate: 28906 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 10

Total time: 10 seconds
Operation rate: 28840 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 10

Total time: 10 seconds
Operation rate: 28064 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 10

Total time: 10 seconds
Operation rate: 28313 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 10

Total time: 10 seconds
Operation rate: 28533 ops/s


_________________________________________________

Menos Remoçoes e mais gets
_________________________________________________

java -cp bin  cp.articlerep.MainRep 10 16 1000 10 5 85 6 30 10

Total time: 10 seconds
Operation rate: 44779 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 10 5 85 6 30 10

Total time: 10 seconds
Operation rate: 44663 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 10 5 85 6 30 10

Total time: 10 seconds
Operation rate: 45308 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 10 5 85 6 30 10

Total time: 10 seconds
Operation rate: 45363 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 10 5 85 6 30 10

Total time: 10 seconds
Operation rate: 44390 ops/s


_________________________________________________

Menos Remoçoes e mais puts
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 16 1000 50 10 40 6 30 10

Total time: 10 seconds
Operation rate: 23020 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 50 10 40 6 30 10

Total time: 10 seconds
Operation rate: 22847 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 50 10 40 6 30 10

Total time: 10 seconds
Operation rate: 22538 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 50 10 40 6 30 10

Total time: 10 seconds
Operation rate: 22298 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 50 10 40 6 30 10

Total time: 10 seconds
Operation rate: 22746 ops/s



_________________________________________________

Listas de tamanho maior
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 100

Total time: 10 seconds
Operation rate: 9672 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 100

Total time: 10 seconds
Operation rate: 9589 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 100

Total time: 10 seconds
Operation rate: 9709 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 100

Total time: 10 seconds
Operation rate: 9937 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 100

Total time: 10 seconds
Operation rate: 9495 ops/s


_________________________________________________

Listas do tamanho do numero de palavras
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 16 1000 25 25 50 6 30 1000

Total time: 10 seconds
Operation rate: 596 ops/s


_________________________________________________

MUITOS REMOVES poucos puts
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 16 1000 10 40 50 6 30 10

Total time: 10 seconds
Operation rate: 52942 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 10 40 50 6 30 10

Total time: 10 seconds
Operation rate: 51992 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 10 40 50 6 30 10

Total time: 10 seconds
Operation rate: 52920 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 10 40 50 6 30 10

Total time: 10 seconds
Operation rate: 52992 ops/s


java -cp bin  cp.articlerep.MainRep 10 16 1000 10 40 50 6 30 10

Total time: 10 seconds
Operation rate: 53102 ops/s



###############################################################################

8 Threads

###############################################################################


_________________________________________________

Teste de referencia
_________________________________________________

java -cp bin  cp.articlerep.MainRep 10 8 1000 25 25 50 6 30 10

Total time: 10 seconds
Operation rate: 29882 ops/s


_________________________________________________

Menos Remoçoes e mais gets
_________________________________________________

java -cp bin  cp.articlerep.MainRep 10 8 1000 10 5 85 6 30 10

Total time: 10 seconds
Operation rate: 48838 ops/s


_________________________________________________

Menos Remoçoes e mais puts
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 8 1000 50 10 40 6 30 10

Total time: 10 seconds
Operation rate: 24088 ops/s


_________________________________________________

Listas de tamanho maior
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 8 1000 25 25 50 6 30 100

Total time: 10 seconds
Operation rate: 10373 ops/s


_________________________________________________

Listas do tamanho do numero de palavras
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 8 1000 25 25 50 6 30 1000

Total time: 10 seconds
Operation rate: 290 ops/s


_________________________________________________

MUITOS REMOVES poucos puts
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 8 1000 10 40 50 6 30 10

Total time: 10 seconds
Operation rate: 55648 ops/s



###############################################################################

4 Threads

###############################################################################


_________________________________________________

Teste de referencia
_________________________________________________

java -cp bin  cp.articlerep.MainRep 10 4 1000 25 25 50 6 30 10

Total time: 10 seconds
Operation rate: 32730 ops/s


_________________________________________________

Menos Remoçoes e mais gets
_________________________________________________

java -cp bin  cp.articlerep.MainRep 10 4 1000 10 5 85 6 30 10

Total time: 10 seconds
Operation rate: 46609 ops/s


_________________________________________________

Menos Remoçoes e mais puts
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 4 1000 50 10 40 6 30 10

Total time: 10 seconds
Operation rate: 25604 ops/s


_________________________________________________

Listas de tamanho maior
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 4 1000 25 25 50 6 30 100

Total time: 10 seconds
Operation rate: 9435 ops/s


_________________________________________________

Listas do tamanho do numero de palavras
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 4 1000 25 25 50 6 30 1000

Total time: 10 seconds
Operation rate: 118 ops/s


_________________________________________________

MUITOS REMOVES poucos puts
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 4 1000 10 40 50 6 30 10

Total time: 10 seconds
Operation rate: 59247 ops/s




###############################################################################

32 Threads

###############################################################################


_________________________________________________

Teste de referencia
_________________________________________________

java -cp bin  cp.articlerep.MainRep 10 32 1000 25 25 50 6 30 10

Total time: 10 seconds
Operation rate: 27292 ops/s


_________________________________________________

Menos Remoçoes e mais gets
_________________________________________________

java -cp bin  cp.articlerep.MainRep 10 32 1000 10 5 85 6 30 10

Total time: 10 seconds
Operation rate: 43393 ops/s


_________________________________________________

Menos Remoçoes e mais puts
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 32 1000 50 10 40 6 30 10

Total time: 10 seconds
Operation rate: 22550 ops/s


_________________________________________________

Listas de tamanho maior
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 32 1000 25 25 50 6 30 100

Total time: 10 seconds
Operation rate: 9438 ops/s


_________________________________________________

Listas do tamanho do numero de palavras
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 32 1000 25 25 50 6 30 1000

Total time: 10 seconds
Operation rate: 560 ops/s


_________________________________________________

MUITOS REMOVES poucos puts
_________________________________________________


java -cp bin  cp.articlerep.MainRep 10 32 1000 10 40 50 6 30 10

Total time: 10 seconds
Operation rate: 50579 ops/s