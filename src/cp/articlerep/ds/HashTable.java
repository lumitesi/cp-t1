package cp.articlerep.ds;

import java.util.concurrent.locks.*;

/**
 * @author Ricardo Dias
 */
public class HashTable<K extends Comparable<K>, V> implements Map<K, V> {

	private static class Node {
		public Object key;
		public Object value;
		public Node next;

		public Node(Object key, Object value, Node next) {
			this.key = key;
			this.value = value;
			this.next = next;
		}
	}

	private Node[] table;

	private ReentrantReadWriteLock[] locks;
	private Lock acquireLock, releaseLock;	

	public HashTable() {
		this(1000);
	}

	public HashTable(int size) {
		this.table = new Node[size];
		this.locks = new ReentrantReadWriteLock[size];
		acquireLock = new ReentrantLock();
		releaseLock = new ReentrantLock();
	}

	private int calcTablePos(K key) {
		return Math.abs(key.hashCode()) % this.table.length;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V put(K key, V value) {
		int pos = this.calcTablePos(key);
		Node n = this.table[pos];

		while (n != null && !n.key.equals(key)) {
			n = n.next;
		}
		if (n != null) {
			V oldValue = (V) n.value;
			n.value = value;
			return oldValue;
		}

		Node nn = new Node(key, value, this.table[pos]);
		this.table[pos] = nn;
		return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public V remove(K key) {
		int pos = this.calcTablePos(key);
		Node p = this.table[pos];

		if (p == null) {
			return null;
		}

		if (p.key.equals(key)) {
			this.table[pos] = p.next;
			return (V) p.value;
		}

		Node n = p.next;
		while (n != null && !n.key.equals(key)) {
			p = n;
			n = n.next;
		}

		if (n == null) {
			return null;
		}

		p.next = n.next;

		return (V) n.value;

	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(K key) {
		int pos = this.calcTablePos(key);
		Node n = this.table[pos];

		while (n != null && !n.key.equals(key)) {
			n = n.next;
		}
		return (V) (n != null ? n.value : null);

	}

	@Override
	public boolean contains(K key) {
		return get(key) != null;
	}

	/**
	 * No need to protect this method from concurrent interactions
	 */
	@Override
	public Iterator<V> values() {
		return new Iterator<V>() {

			private int pos = -1;
			private Node nextBucket = advanceToNextBucket();

			private Node advanceToNextBucket() {
				pos++;
				while (pos < HashTable.this.table.length
						&& HashTable.this.table[pos] == null) {
					pos++;
				}
				if (pos < HashTable.this.table.length)
					return HashTable.this.table[pos];

				return null;
			}

			@Override
			public boolean hasNext() {
				return nextBucket != null;
			}

			@SuppressWarnings("unchecked")
			@Override
			public V next() {
				V result = (V) nextBucket.value;

				nextBucket = nextBucket.next != null ? nextBucket.next
						: advanceToNextBucket();

				return result;
			}

		};
	}

	@Override
	public Iterator<K> keys() {
		return null;
	}

	/**
	 * Locks a list for write operations. The list is located in the position of
	 * the hashtable where the given key is.
	 * 
	 * @param key
	 *            key for the value to access.
	 */
	public void acquireWriteLockKey(K key) {
		int pos = calcTablePos(key);
		ReentrantReadWriteLock lock = getLock(pos);
		lock.writeLock().lock();
	}

	/**
	 * Locks various lists for write operations. Each list is located in the
	 * position of the hashtable where each given key is.
	 * 
	 * @param keys
	 *            list of keys for the values to access.
	 */
	public void acquireWriteLockKey(List<K> keys) {
		acquireLock.lock();
		Iterator<K> it = keys.iterator();
		while (it.hasNext()) {
			int pos = calcTablePos(it.next());
			ReentrantReadWriteLock lock = getLock(pos);
			lock.writeLock().lock();
		}
		acquireLock.unlock();
	}

	/**
	 * Locks a list for read operations. The list is located in the position of
	 * the hashtable where the given key is.
	 * 
	 * @param key
	 *            key for the value to access.
	 */
	public void acquireReadLockKey(K key) {
		int pos = calcTablePos(key);
		ReentrantReadWriteLock lock = getLock(pos);
		lock.readLock().lock();
	}

	/**
	 * Locks various lists for read operations. Each list is located in the
	 * position of the hashtable where each given key is.
	 * 
	 * @param keys
	 *            list of keys for the values to access.
	 */
	public void acquireReadLockKey(List<K> keys) {
		acquireLock.lock();
		Iterator<K> it = keys.iterator();
		while (it.hasNext()) {
			int pos = calcTablePos(it.next());
			ReentrantReadWriteLock lock = getLock(pos);
			lock.readLock().lock();
		}
		acquireLock.unlock();
	}

	/**
	 * Unlocks a previously acquired writeLock for a list located in the
	 * position of the hashtable where the given key is.
	 * 
	 * @param key
	 *            key for the value to access.
	 */
	public void releaseWriteLockKey(K key) {
		int pos = calcTablePos(key);
		ReentrantReadWriteLock lock = getLock(pos);
		try {
			lock.writeLock().unlock();
		} catch (IllegalMonitorStateException e) {
			// Do nothing, just catch the exception
		}
	}

	/**
	 * Unlocks a previously acquired writeLock for each list located in the
	 * hashtable where each given key is.
	 * 
	 * @param keys
	 *            list of keys for the values to access.
	 */
	public void releaseWriteLockKey(List<K> keys) {
		releaseLock.lock();
		Iterator<K> it = keys.iterator();
		while (it.hasNext()) {
			int pos = calcTablePos(it.next());
			ReentrantReadWriteLock lock = getLock(pos);
			try {
				lock.writeLock().unlock();
			} catch (IllegalMonitorStateException e) {
				// Do nothing, just catch the exception
			}
		}
		releaseLock.unlock();
	}

	/**
	 * Unlocks a previously acquired readLock for a list located in the position
	 * of the hashtable where the given key is.
	 * 
	 * @param key
	 *            key for the value to access.
	 */
	public void releaseReadLockKey(K key) {
		int pos = calcTablePos(key);
		ReentrantReadWriteLock lock = getLock(pos);
		try {
			lock.readLock().unlock();
		} catch (IllegalMonitorStateException e) {
			// Do nothing, just catch the exception
		}
	}

	/**
	 * Unlocks a previously acquired readLock for each list located in the
	 * hashtable where each given key is.
	 * 
	 * @param keys
	 *            list of keys for the values to access.
	 */
	public void releaseReadLockKey(List<K> keys) {
		releaseLock.lock();
		Iterator<K> it = keys.iterator();
		while (it.hasNext()) {
			int pos = calcTablePos(it.next());
			ReentrantReadWriteLock lock = getLock(pos);
			try {
				lock.readLock().unlock();
			} catch (IllegalMonitorStateException e) {
				// Do nothing, just catch the exception
			}
		}
		releaseLock.unlock();
	}

	/**
	 * Gets a lock associated with a list from the hashtable.
	 * 
	 * @param index
	 *            index of the list.
	 * @return lock object.
	 */
	private ReentrantReadWriteLock getLock(int index) {
		if (locks[index] == null)
			locks[index] = new ReentrantReadWriteLock(true);
		return locks[index];
	}
}
