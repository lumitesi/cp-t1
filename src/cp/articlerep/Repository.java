package cp.articlerep;

import java.util.HashSet;

import cp.articlerep.ds.Iterator;
import cp.articlerep.ds.LinkedList;
import cp.articlerep.ds.List;
import cp.articlerep.ds.Map;
import cp.articlerep.ds.HashTable;

/**
 * @author Ricardo Dias
 */
public class Repository {

	private Map<String, List<Article>> byAuthor;
	private Map<String, List<Article>> byKeyword;
	private Map<Integer, Article> byArticleId;

	public Repository(int nkeys) {
		this.byAuthor = new HashTable<String, List<Article>>(nkeys * 2);
		this.byKeyword = new HashTable<String, List<Article>>(nkeys * 2);
		this.byArticleId = new HashTable<Integer, Article>(nkeys * 2);
	}

	public boolean insertArticle(Article a) {
		try {
			// Locks all resources before doing any change
			lockAllResourcesToWrite(a);

			if (byArticleId.contains(a.getId()))
				return false;

			Iterator<String> authors = a.getAuthors().iterator();
			while (authors.hasNext()) {
				String name = authors.next();

				List<Article> ll = byAuthor.get(name);
				if (ll == null) {
					ll = new LinkedList<Article>();
					byAuthor.put(name, ll);
				}
				ll.add(a);
			}

			Iterator<String> keywords = a.getKeywords().iterator();
			while (keywords.hasNext()) {
				String keyword = keywords.next();

				List<Article> ll = byKeyword.get(keyword);
				if (ll == null) {
					ll = new LinkedList<Article>();
					byKeyword.put(keyword, ll);
				}
				ll.add(a);
			}

			byArticleId.put(a.getId(), a);

		} finally {
			// Unlocks all the previously locked resources.
			unLockAllResourcesToWrite(a);
		}
		return true;
	}

	public void removeArticle(int id) {
		Article a = byArticleId.get(id);
		try {
			if (a == null)
				return;
			lockAllResourcesToWrite(a);

			Iterator<String> authors = a.getAuthors().iterator();
			while (authors.hasNext()) {
				String name = authors.next();

				List<Article> ll = byAuthor.get(name);
				if (ll != null) {
					int pos = 0;
					Iterator<Article> it = ll.iterator();
					while (it.hasNext()) {
						Article toRem = it.next();
						if (toRem == a) {
							break;
						}
						pos++;
					}
					ll.remove(pos);
					it = ll.iterator();
					if (!it.hasNext()) { // checks if the list is empty
						byAuthor.remove(name);
					}
				}
			}

			Iterator<String> keywords = a.getKeywords().iterator();
			while (keywords.hasNext()) {
				String keyword = keywords.next();

				List<Article> ll = byKeyword.get(keyword);
				if (ll != null) {
					int pos = 0;
					Iterator<Article> it = ll.iterator();
					while (it.hasNext()) {
						Article toRem = it.next();
						if (toRem == a) {
							break;
						}
						pos++;
					}
					ll.remove(pos);
					it = ll.iterator();
					if (!it.hasNext()) { // checks if the list is empty
						byKeyword.remove(keyword);
					}
				}
			}

			byArticleId.remove(id);
		} finally {
			if (a != null)
				// Unlocks all the previously locked resources.
				unLockAllResourcesToWrite(a);
		}
	}

	public List<Article> findArticleByAuthor(List<String> authors) {
		try {
			// Locks all the authors in the byAuthor table that are mentioned in
			// the given list
			((HashTable<String, List<Article>>) byAuthor)
					.acquireReadLockKey(authors);

			List<Article> res = new LinkedList<Article>();
			Iterator<String> it = authors.iterator();
			while (it.hasNext()) {
				String name = it.next();
				List<Article> as = byAuthor.get(name);
				if (as != null) {
					Iterator<Article> ait = as.iterator();
					while (ait.hasNext()) {
						Article a = ait.next();
						res.add(a);
					}
				}
			}

			return res;
		} finally {
			// Unlocks all the authors in the byAuthor table that are mentioned
			// in the given list
			((HashTable<String, List<Article>>) byAuthor)
					.releaseReadLockKey(authors);
		}
	}

	public List<Article> findArticleByKeyword(List<String> keywords) {
		try {
			// Locks all the keywords in the byKeyword table that are mentioned
			// in
			// the given list
			((HashTable<String, List<Article>>) byKeyword)
					.acquireReadLockKey(keywords);

			List<Article> res = new LinkedList<Article>();

			Iterator<String> it = keywords.iterator();
			while (it.hasNext()) {
				String keyword = it.next();
				List<Article> as = byKeyword.get(keyword);
				if (as != null) {
					Iterator<Article> ait = as.iterator();
					while (ait.hasNext()) {
						Article a = ait.next();
						res.add(a);
					}
				}
			}

			return res;
		} finally {
			// Unlocks all the keywords in the byKeyword table that are
			// mentioned
			// in the given list
			((HashTable<String, List<Article>>) byKeyword)
					.releaseReadLockKey(keywords);
		}

	}

	/**
	 * This method is supposed to be executed with no concurrent thread
	 * accessing the repository.
	 * 
	 */
	public boolean validate() {

		HashSet<Integer> articleIds = new HashSet<Integer>();
		int articleCount = 0;

		Iterator<Article> aIt = byArticleId.values();
		while (aIt.hasNext()) {
			Article a = aIt.next();

			articleIds.add(a.getId());
			articleCount++;

			// check the authors consistency
			Iterator<String> authIt = a.getAuthors().iterator();
			while (authIt.hasNext()) {
				String name = authIt.next();
				if (!searchAuthorArticle(a, name)) {
					return false;
				}
			}

			// check the keywords consistency
			Iterator<String> keyIt = a.getKeywords().iterator();
			while (keyIt.hasNext()) {
				String keyword = keyIt.next();
				if (!searchKeywordArticle(a, keyword)) {
					return false;
				}
			}
		}

		return articleCount == articleIds.size();
	}

	private boolean searchAuthorArticle(Article a, String author) {
		List<Article> ll = byAuthor.get(author);
		if (ll != null) {
			Iterator<Article> it = ll.iterator();
			while (it.hasNext()) {
				if (it.next() == a) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean searchKeywordArticle(Article a, String keyword) {
		List<Article> ll = byKeyword.get(keyword);
		if (ll != null) {
			Iterator<Article> it = ll.iterator();
			while (it.hasNext()) {
				if (it.next() == a) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Locks all needed resources to make changes related to a given Article.
	 * 
	 * @param a
	 *            article to make changes about.
	 */
	private void lockAllResourcesToWrite(Article a) {
		// Locks the line in the byArticleId table where the given ID exists in
		// order to perform changes to an article with
		((HashTable<Integer, Article>) byArticleId).acquireWriteLockKey(a
				.getId());

		// Locks all the lines in the byAuthors table where the authors
		// mentioned in the article exist.
		((HashTable<String, List<Article>>) byAuthor).acquireWriteLockKey(a
				.getAuthors());

		// Locks all the lines in the byKeywords table where the keywords
		// mentioned in the article exist.
		((HashTable<String, List<Article>>) byKeyword).acquireWriteLockKey(a
				.getKeywords());

	}

	/**
	 * Unlocks all needed resources previously locked to write operations.
	 * 
	 * @param a
	 *            article to make changes about.
	 */
	private void unLockAllResourcesToWrite(Article a) {
		// Unlocks all the lines in the byKeywords table where the keywords
		// mentioned in the article exist.
		((HashTable<String, List<Article>>) byKeyword).releaseWriteLockKey(a
				.getKeywords());

		// Unlocks all the lines in the byAuthors table where the authors
		// mentioned in the article exist.
		((HashTable<String, List<Article>>) byAuthor).releaseWriteLockKey(a
				.getAuthors());

		// Unlocks the line in the byArticleId table where the given ID exists
		// in
		// order to perform changes to an article with
		((HashTable<Integer, Article>) byArticleId).releaseWriteLockKey(a
				.getId());
	}
}
